var path = require('path');

var DocumentDBClient = require('documentdb').DocumentClient;
var docdbUtils = require('./docdbUtils');

var fs = require('fs');

function TaskDao(documentDBClient, databaseId, collectionId) {
  this.client = documentDBClient;
  this.databaseId = databaseId;
  this.collectionId = collectionId;

  this.database = null;
  this.collection = null;
}

module.exports = TaskDao;

TaskDao.prototype = {
    init: function (callback) {
        var self = this;

        docdbUtils.getOrCreateDatabase(self.client, self.databaseId, function (err, db) {
            if (err) {
                callback(err);

            } else {
                self.database = db;
                docdbUtils.getOrCreateCollection(self.client, self.database._self, self.collectionId, function (err, coll) {
                    if (err) {
                        callback(err);

                    } else {
                        self.collection = coll;
                    }
                });
            }
        });
    },

    find: function (querySpec, callback) {
        var self = this;

        self.client.queryDocuments(self.collection._self, querySpec).toArray(function (err, results) {
            if (err) {
                callback(err);

            } else {
                callback(null, results);
            }
        });
    },

    addItem: function (item, callback) {
        var self = this;
 
		fs.readFile(path.join(__dirname, '/screenlog.0'), 'utf8', function (err, content) {
		 	if (err) {
		    	return callback(err);
		    }
            else
            {

                console.log('cat returned some content: ' + content);
                var tmp1 = content.split(" ")
                console.log('tmp1 content: ' + tmp1);


                var item = {
        		    Address: "$00000",
        		    DeviceID: "17564321",
        		    Time: Date(),
        		    LightValue: tmp1[tmp1.length - 5],
                    TempValue: tmp1[tmp1.length - 3],
                    HumidValue: tmp1[tmp1.length-1]                   
                };

    	        self.client.createDocument(self.collection._self, item, function (err, doc) {
        	        if (err) {
        	           callback(err);
        	        } 
                    else 
                    {
        	            console.log(new Date(), 'Uploaded: ' + item.Address + ' ' + item.Time + ' ' + item.LightValue + ' ' + item.TempValue + ' ' + item.HumidValue);
                       
        	        }
    	        });
            }

        });
       
    },

    updateItem: function (itemId, callback) {
        var self = this;

        self.getItem(itemId, function (err, doc) {
            if (err) {
                callback(err);

            } else {
                doc.completed = true;

                self.client.replaceDocument(doc._self, doc, function (err, replaced) {
                    if (err) {
                        callback(err);

                    } else {
                        callback(null, replaced);
                    }
                });
            }
        });
    },

    getItem: function (itemId, callback) {
        var self = this;

        var querySpec = {
            query: 'SELECT * FROM root r WHERE r.id=@id',
            parameters: [{
                name: '@id',
                value: itemId
            }]
        };

        self.client.queryDocuments(self.collection._self, querySpec).toArray(function (err, results) {
            if (err) {
                callback(err);

            } else {
                callback(null, results[0]);
            }
        });
    }
};