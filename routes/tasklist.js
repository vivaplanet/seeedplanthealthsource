var DocumentDBClient = require('documentdb').DocumentClient;
var async = require('async');
var cron = require('node-schedule');

function TaskList(taskDao) {
  this.taskDao = taskDao;
}

module.exports = TaskList;

TaskList.prototype = {
    showTasks: function (req, res) {
        var self = this;

        var querySpec = 
        {

            query: 'SELECT d.Address, d.Time, d.LightValue, d.TempValue, d.HumidValue FROM OpenDevices d WHERE d.Address=@SensorType', //d.DeviceSensors[1].SensorType=@SensorType',     
            parameters: [          
                {name: '@SensorType', value: '$00000'}          
            ] 

            /*query: 'SELECT * FROM  OpenDevices r'*/
        };


        self.taskDao.find(querySpec, function (err, items) 
        {
            if (err) 
            {
                callback(err);
            }

            res.render('index', {
                title: 'My Environment Information',
                tasks: items,
                JSONTasks: JSON.stringify(items)
            });
        });
    },

    addTask: function (req, res) {
        var self = this;
        var item;
        var rule = new cron.RecurrenceRule();
        rule.minute = new cron.Range(0, 59, 3);//should update every 3 mins
        // rule.second = 30;

        cron.scheduleJob(rule, function()
        {
          
            self.taskDao.addItem(item, function (err) {
                if (err) {
                    throw (err);
                }

                res.redirect('/');
            });

        });

    },

    completeTask: function (req, res) {
        var self = this;
        var completedTasks = Object.keys(req.body);

        async.forEach(completedTasks, function taskIterator(completedTask, callback) {
            self.taskDao.updateItem(completedTask, function (err) {
                if (err) {
                    callback(err);
                } else {
                    callback(null);
                }
            });
        }, function goHome(err) {
            if (err) {
                throw err;
            } else {
                res.redirect('/');
            }
        });
    }
};